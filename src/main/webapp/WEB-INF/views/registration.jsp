
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>REGISTRATION-PAGE</title>
</head>
<body>
	<div align="center">
		<h1>Registration</h1>
		<div class="jumbotron">
			<form:form action="login" method="post" modelAttribute="user">

				<table style="border-collapse: separate; border-spacing: 0 1em;">
				
					<tr>
						<td>Name:</td>
						<td><form:input path="userName" /> <form:errors
								path="userName"></form:errors></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><form:input path="emailId" /> <form:errors
								path="emailId"></form:errors></td>
					</tr>
					<tr>
						<td>mobileNumber:</td>
						<td><form:input path="mobileNumber" /> <form:errors
								path="mobileNumber"></form:errors></td>
					</tr>
					<tr>
						<td>address:</td>
						<td><form:input path="address" /> <form:errors
								path="address"></form:errors></td>
					</tr>
					<tr>
						<td>password:</td>
						<td><form:password path="password" /> <form:errors
								path="password"></form:errors></td>
					</tr>
					

					<tr>
						<td colspan="2" align="center"><input type="submit"
							class="btn btn-success" value="Register"></td>
					</tr>
				</table>
				
			</form:form>
		</div>
	</div>
</body>
</html>