package com.main.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.main.model.Products;

public interface ProductRepsitory extends CrudRepository<Products, Integer> {

	List<Products> findByCategory(String category);

	Products findById(int id);

}
