package com.main.repository;

import org.springframework.data.repository.CrudRepository;

import com.main.model.AddToCart;

public interface AddToCartRepository extends CrudRepository<AddToCart,Integer>{

}
