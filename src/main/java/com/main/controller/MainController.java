package com.main.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.main.model.AddToCart;
import com.main.model.Products;
import com.main.model.User;
import com.main.repository.AddToCartRepository;
import com.main.repository.ProductRepsitory;
import com.main.repository.UserRepository;

@RestController
public class MainController {
	@Autowired
	UserRepository userRepository;
	@Autowired
	ProductRepsitory productrepo;
	@Autowired
	AddToCartRepository addCart;
	
	@RequestMapping("/")
	public ModelAndView homePage() {
		
		return new ModelAndView("firstpage");
	}	
	
	@RequestMapping("login")
	public ModelAndView loginPage(@ModelAttribute("user") User user) {
		userRepository.save(user);
		return new ModelAndView("login");
	}
	@RequestMapping("registration")
	public ModelAndView registerUser(@ModelAttribute("user") User user) {
	
		return new ModelAndView("registration");
	}

	/*@RequestMapping("save")
	public ModelAndView saveUser(@ModelAttribute("user") User user) {
		userRepository.save(user);
		return new ModelAndView("sucess");
	}*/
	@RequestMapping("/admin")
	public ModelAndView adminDetails(@ModelAttribute("user")Products toy) {
		//userRepository.save(user);
		return new ModelAndView("admin");
	}
	@RequestMapping("homepage")
	public ModelAndView loginCondition( User user, HttpServletRequest request) {
	
		String name = request.getParameter("userName");
		String password=request.getParameter("password");
		List<User> userList=(List<User>) userRepository.findAll();
		for(User userDetails:userList)
		{
			if(userDetails.getUserName().equals(name)&&userDetails.getPassword().equals(password))
			{
				return new ModelAndView("homepage");
			}
		}
		return new ModelAndView("login");
	}
	
	@RequestMapping("toys")
	public ModelAndView addToyDetails(@ModelAttribute("user") Products product) {
	
		//toyrepo.save(toys);
		return new ModelAndView("product");
	}
	@RequestMapping("save")
	public ModelAndView saveToyDetails(@ModelAttribute("user") Products product) {
	
	productrepo.save(product);
		return new ModelAndView("admin");
	}
	@RequestMapping("viewToys")
	public ModelAndView viewToyDetails(@ModelAttribute("user") Products product) {
	
		
		//toyrepo.save(toys);
		List<Products> productlist=productrepo.findByCategory("toys");	
		return new ModelAndView("success","productList",productlist);
	}
	
	@RequestMapping("AddToCart/{id}")
	public ModelAndView viewAddToCart(@PathVariable int id,Products product) {
	
		Products product1=productrepo.findById(id);
		//addCart.save(product1);
		//toyrepo.save(toys);
		//List<AddToCart> productlist=productrepo.findByCategory("toys");	
		List<AddToCart> productlist= (List<AddToCart>) addCart.findAll();
		return new ModelAndView("success","productlist",productlist);
	}
	
	
	
	
	
	
	
	
	
}
