package com.main.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AddToCart {
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int cartid;
private int pid;
public int getCartid() {
	return cartid;
}
public void setCartid(int cartid) {
	this.cartid = cartid;
}
public int getPid() {
	return pid;
}
public void setPid(int pid) {
	this.pid = pid;
}

}
